<?php
class Database{
	private $connection;
	protected static $_instance = null;

	public static function getInstance(){
		if(null === self::$_instance){
			self::$_instance = new self;
		}
		return self::$_instance;
	}

	public function getConnection(){
		return $this->connection;
	}	

	private function __construct(){
		try{	
			$this->connection = new PDO('mysql:dbname=oop_cms;host=localhost','root','root');
		}catch(PDOExeption $e){
			echo 'Connection failed: '. $e->getMessage();
		}
	}

	private function __clone(){}

	public function query($sql){
		$result = $this->connection->prepare($sql);
		$result->execute();		
		return $result;
	}	

	public function prepare($sql){
		return $this->connection->prepare($sql);
	}	

	public function lastInsertId(){
		return $this->connection->lastInsertId();
	}
}
?>