<?php
	
class databaseObject{

	public static function find_all(){
		$db = Database::getInstance();
		$sql = "SELECT * FROM ". static::$table_name;
		$result_set = $db->query($sql);
		return $result_set->fetchall(PDO::FETCH_CLASS);
	}

	public static function find_by_id($id){
		$db = Database::getInstance();
		$sql = "SELECT * from ". static::$table_name . " WHERE id = " . $id;
		$result_set = $db->query($sql);
		$result = $result_set->fetchobject(get_called_class());
		return $result;
	}

	protected function attributes(){
		$attributes = [];
		foreach(static::$db_fields as $field){
			if(property_exists(get_called_class(), $field)){
				$attributes[$field] = $this->$field;
			}
		}
		return $attributes;
	}

	public function create(){
		$db = Database::getInstance();
		$attributes = $this->attributes();
		$sql  = "INSERT INTO ". static::$table_name ." (";
		$sql .= join(", ", array_keys($attributes));
		$sql .= ") VALUES ('";
		$sql .= join("', '", array_values($attributes));
		$sql .= "')";
				
		$db->query($sql);
		$this->id = $db->lastInsertId();
				
	}	

	public function update(){
		$db = Database::getInstance();
		$attribute_pairs = [];
		$attributes = $this->attributes();
		foreach($attributes as $key => $value ){
			$attribute_pairs[] = "{$key} ='{$value}'";
		}		
		$sql  = "UPDATE ". static::$table_name. " SET ";
		$sql .= join(", ", $attribute_pairs);
		$sql .= " WHERE id = ". $this->id;
		return $db->query($sql);
	}

	public function delete(){
		$db = Database::getInstance();
		$sql  = "DELETE FROM ". static::$table_name;
		$sql .= " WHERE id = ". $this->id;
		$sql .= " LIMIT 1";
		return $db->query($sql);
	}
}

?>