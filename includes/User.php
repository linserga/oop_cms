<?php

class User extends databaseObject{
	protected static $table_name = 'users';
	protected static $db_fields = ['id','username','password', 'group_id'];

	public $id;
	public $username;
	public $password;
	public $group_id;

	public static function authenticate($username="", $password=""){
		$db = Database::getInstance();

		$sql  = "SELECT * FROM users ";
		$sql .= "WHERE username = '{$username}' ";
		$sql .= "AND password = '{$password}' ";
		$sql .= "LIMIT 1";

		$result_set = $db->query($sql);
		$result = $result_set->fetchobject("User");
		return !empty($result) ? $result : false;
	}

	public static function is_admin($user){
		$db = Database::getInstance();
		$sql  = "SELECT id FROM groups ";
		$sql .= "WHERE id = {$user->group_id} AND id = 1 ";
		$sql .= "LIMIT 1";
		$result_set = $db->query($sql);
		$result = $result_set->fetch();		
		return !empty($result) ? $result : false; 
	}
}

?>