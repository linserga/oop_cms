<?php

class Post extends databaseObject{
	protected static $table_name = "posts";
	protected static $db_fields = ['id', 'subject_id', 'name', 'content'];

	public $id;
	public $subject_id;
	public $name;
	public $content;
}

?>