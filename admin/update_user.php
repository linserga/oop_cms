<?php require_once "../includes/functions.php";?>
<?php
if(isset($_GET['id'])){
	$user = User::find_by_id($_GET['id']);
}else{
	redirect_to("index.php");
}

if(isset($_POST['submit'])){
	if(!empty($_POST['id']) && !empty($_POST['username']) && !empty($_POST['password'])){
		$user->id = $_POST['id'];
		$user = User::find_by_id($_POST['id']);	
		$user->username = $_POST['username'];	
		$user->password = $_POST['password'];
		$user->update();
		redirect_to("index.php");
	}else{
		$message = "Please fill in correct form";
		redirect_to("index.php");
	}
}
?>
<form action="" method="POST">
	<label for="Name">Name</label>
	<input type="text" name="username" value="<?php echo isset($user->username) ? $user->username : '';?>">
	<label for="password">Password</label>
	<input type="text" name="password" value="<?php echo isset($user->password) ? $user->password : '';?>">
	<br>
	<input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
	<input type="submit" name="submit" value="Update">
</form>