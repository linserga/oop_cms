<?php ob_start();?>
<?php spl_autoload_register(function($class){
	require_once $_SERVER['DOCUMENT_ROOT']."/oop_cms/includes/".$class.".php";
})
?>
<?php require_once "../includes/functions.php";?>
<?php 
	$session = Session::getSession(); 	
	if($session->is_logged_in()){redirect_to("index.php");}?>
<?php
	if(isset($_POST['submit'])){		
		$username = $_POST['username'];
		$password = $_POST['password'];

		if(!empty($username) && !empty($password)){

			$found_user = User::authenticate($username, $password);
			
			if($found_user){
				$admin = User::is_admin($found_user);
				if($admin){
					$session->login($found_user);
					redirect_to("index.php");
				}
			}else{
				$message = "Username/password combination incorrect or you are not allowed to visit admin area.";
			}
		}else{
			$username = "";
			$password = "";
			$message = "Enter valid Username/password";
		}
	}
?>
<?php require_once "../includes/layouts/admin_header.php";?>
<?php echo isset($message) ? output_message($message) : ""; ?>
<form action="" method="POST">
	<label for="username">Username</label>
	<input type="text" name="username" value="">
	<br>
	<label for="password">Password</label>
	<input type="text" name="password">
	<br>
	<input type="submit" name="submit" value="Login">
</form>
<?php require_once "../includes/layouts/admin_footer.php";?>
