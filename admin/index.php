<?php ob_start();?>
<?php spl_autoload_register(function($class){
	require_once $_SERVER["DOCUMENT_ROOT"]. "/oop_cms/includes/".$class.".php";
})
?>
<?php require_once "../includes/functions.php";?>
<?php 
	$session = Session::getSession(); 
	$message = $session->message(); 
	if(!$session->is_logged_in()){redirect_to("login.php");}
?>
<?php require_once "../includes/layouts/admin_header.php";?>
<?php require_once "../includes/layouts/nav.php";?>
<?php
	if(isset($_GET['action'])){
		$action = $_GET['action'];
		switch ($action):
			case "create_user":
				require "create_user.php";
				break;
			case "update_user":
				require "update_user.php";
				break;	
			case "delete_user":
				require "delete_user.php";
				break;
			case "logout":
				require "logout.php";
				break;		
		endswitch;
	}else{
		require "default.php";
	}
?>
<?php require_once "../includes/layouts/admin_footer.php";?>
	
