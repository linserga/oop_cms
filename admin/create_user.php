<?php require_once "../includes/functions.php";?>
<?php
if(isset($_POST['submit'])){
	if(!empty($_POST['username']) && !empty($_POST['password'])){
		$user = new User();
		$user->username = $_POST['username'];
		$user->password = $_POST['password'];
		$user->group_id = $_POST['group'];
		$user->create();
		redirect_to("index.php");
	}else{
		$message = "Please fill in form";
		redirect_to("index.php");
	}
}
?>
<form action="" method="POST">
	<label for="Name">Name</label>
	<input type="text" name="username" value="">
	<br>
	<label for="password">Password</label>
	<input type="text" name="password" value="">
	<br>
	<label for="group">Group</label>
	<input type="radio" name="group" value="1">Admin
	<input type="radio" name="group" value="2" checked>User
	<br>
	<input type="submit" name="submit" value="Create">
</form>