<?php 
	ob_start();
	spl_autoload_register(function($class){	require_once "/home/serge/sites/oop_cms/includes/".$class.".php";});

	require_once "../includes/functions.php";
	$session = Session::getSession();
	$session->logout();
	redirect_to("login.php");
?>